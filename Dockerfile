FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/fuelSystemService-0.0.1-SNAPSHOT.jar fuelSystemService-0.0.1-SNAPSHOT.jar
EXPOSE 9004
COPY . .
ENTRYPOINT ["java", "-jar","fuelSystemService-0.0.1-SNAPSHOT.jar"]