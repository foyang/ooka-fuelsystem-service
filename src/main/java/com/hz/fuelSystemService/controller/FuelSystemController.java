package com.hz.fuelSystemService.controller;

import com.hz.fuelSystemService.model.*;
import io.github.resilience4j.retry.annotation.Retry;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@CrossOrigin
@RequestMapping("fuel-system")
public class FuelSystemController {

    private static final String FUELSERVICE = "FuelService";
    RestTemplate restTemplate = new RestTemplate();
    private static final Log LOGGER = LogFactory.getLog(FuelSystemController.class);
    private String currentService;

    @Retry(name = FUELSERVICE, fallbackMethod = "fullback_retry_fuel")
    @PostMapping("valid")
    public ResponseEntity<Valid> checkFuelValue(@RequestBody FuelSystem fuelSystem){
        Valid valid = new Valid();
        if (fuelSystem.getValue().length() >= 12){
            valid.setValid(true);
        }
        OilSystemInfos oilSystemInfos = new OilSystemInfos();
        oilSystemInfos.setFrom("Fuel-System");
        oilSystemInfos.setServiceId(2);
        oilSystemInfos.setIsValid(valid.isValid());
        HttpEntity<OilSystemInfos> request = new HttpEntity<>(oilSystemInfos);
        LOGGER.info("#### Start Communication with Oil-System ###: ");
        currentService = "oil-service";
        ResponseEntity<OilSystemInfos> oilSystemInfosResponseEntity = restTemplate.postForEntity("http://oilsystem-service:9003/oil-system/infos", request, OilSystemInfos.class);
        LOGGER.info("#### Response Code from Oil-System: "+oilSystemInfosResponseEntity.getStatusCode());

        EquipmentPersistSystemInfos equipmentPersistSystemInfos = new EquipmentPersistSystemInfos();
        equipmentPersistSystemInfos.setFrom("Fuel-System");
        equipmentPersistSystemInfos.setServiceId(4);
        equipmentPersistSystemInfos.setIsValid(valid.isValid());
        HttpEntity<EquipmentPersistSystemInfos> request2 = new HttpEntity<>(equipmentPersistSystemInfos);
        LOGGER.info("#### Start Communication with Equipment Persist System ###: ");
        ResponseEntity<EquipmentPersistSystemInfos> equipmentPersistSystemInfosResponseEntity = restTemplate.postForEntity("http://equipment-persist-service:9002/equipment/infos", request2, EquipmentPersistSystemInfos.class);
        LOGGER.info("#### Response Code from Equipment Persist System: "+equipmentPersistSystemInfosResponseEntity.getStatusCode());
        currentService = "persist-service";

        return new ResponseEntity<>(valid,HttpStatus.OK);
    }

    public ResponseEntity<String> fullback_retry_fuel(Exception e){
        if (currentService.equals("oil-service")){
            return new ResponseEntity<String>("The Oil service is down or not responding!!", HttpStatus.REQUEST_TIMEOUT);
        } else {
            return new ResponseEntity<String>("The Equipment Persist service is down or not responding!!",HttpStatus.REQUEST_TIMEOUT);
        }
    }

    @PostMapping("infos")
    public FuelSystemInfos getInfosFromOderService(@RequestBody FuelSystemInfos fuelSystemInfos){
        LOGGER.info("#### start receiving information ####");
        LOGGER.info("#### From: "+fuelSystemInfos.getFrom());
        LOGGER.info("#### Service Id: "+fuelSystemInfos.getServiceId());
        LOGGER.info("#### Service Id: "+fuelSystemInfos.getIsValid());
        LOGGER.info("#### End receiving information ####");
        return fuelSystemInfos;
    }

    @GetMapping("test")
    public String apiTest(){
        return "Fuel-System service work well";
    }
}
