package com.hz.fuelSystemService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FuelSystemServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FuelSystemServiceApplication.class, args);
	}

}
