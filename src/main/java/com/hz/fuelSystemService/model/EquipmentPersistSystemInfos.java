package com.hz.fuelSystemService.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EquipmentPersistSystemInfos {
    private String from;
    private int serviceId;
    private Boolean isValid;
}
