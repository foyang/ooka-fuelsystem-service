package com.hz.fuelSystemService.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Valid {
    private boolean isValid = false;
}
